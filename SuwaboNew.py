import subprocess

##cursor <value x>,<value y> - move cursor to specific coord
##mouse <action> - specify mouse action
##rclick <value x>,<value y> - move mouse to specific coord and right click
##lclick <value x>,<value y> - move mouse to specific coord and left click
##loop <int> - looping between loop until endloop
##endloop - stop looping command
##testing

list_of_command = ["cursor", "mouse", "rclick", "lclick"]

def new_script(script):
    subprocess.call('cls',shell=True)
    
    print("Welcome to script editor!")
    print("You can learn more command to make script with 'help'")
    print("Dont forget to use 'quit' to save script and quit editor\n")

    script_array = []
    
    while True:
        try:
            command = input("Input: ")

            if command == "help":
                print_help()

            elif command == "quit":
                print("Saving and quitting editor")
                break

            else:
                if command in list_of_command:
                    print("Found it!")

                else:
                    print("Dont recognized '" + command + "', for more help type 'help'")

        except KeyboardInterrupt as e:
            print("Quitting editor without saving")
            break
            
        except Exception as e:
            print(e)

def print_help():
    print("\n")
    print("cursor <value x>,<value y> - move cursor to specific coord")
    print("mouse <action> - specify mouse action")
    
    print("rclick <value x>,<value y> - move mouse to specific coord and right click")
    print("lclick <value x>,<value y> - move mouse to specific coord and left click")

    print("quit - to save script and quit editor")
    print("help - to see list of command")
    print("\n")
